#imports
from landsat.downloader import Downloader
from landsat.search import Search
from landsat.image import Simple
from os.path import join
import urllib
import requests.packages.urllib3
requests.packages.urllib3.disable_warnings()

#search params
path = None#210
row = None#50
latitude = 52.651282
longitude = -0.480216
s_date = '2015-06-01'
e_date='2016-01-20'
return_scenes = 100
max_cloud_prcnt = 20

srcher = Search()
dwner = Downloader(verbose=True, download_dir=r"LANDSAT-temp")

if path == None and row == None:
    candidate_scenes = srcher.search(lat=latitude, 
                                lon=longitude, 
                                start_date=s_date, 
                                end_date=e_date, 
                                cloud_min=0, 
                                cloud_max=max_cloud_prcnt, 
                                limit=return_scenes)

if candidate_scenes["status"] == "SUCCESS":
    first = candidate_scenes['results'][0]['sceneID']
    for scene_image in candidate_scenes["results"]:
        urllib.urlretrieve(str(scene_image['thumbnail']), 'Images/%s.jpg'%(str(scene_image['sceneID'])))
        print (str(scene_image['thumbnail']))
        #print "Downloading:" , (str(scene_image['sceneID']))
        #dwner.download([str(scene_image['sceneID'])])
        #Simple(join(r"LANDSAT-temp",str(scene_image['sceneID'])+".tar.bz"))